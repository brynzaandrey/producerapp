package com.example.ProducerApp.controller;

import com.example.ProducerApp.dto.MyMessage;
import com.example.ProducerApp.kafka.KafkaSenderExample;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/message")
public class MessageController {

    @Value(value = "${topic.name}")
    private String topicName;

    final KafkaSenderExample kafkaSender;

    @GetMapping("/hello")
    public void sendMessage() {
//        kafkaSender.sendMessage(new MyMessage(1, "Hello", "ConsumerApp"), topicName);
//        kafkaSender.sendMessage(new MyMessage(2, "Hello", "World"), topicName);
//        kafkaSender.sendMessage(new MyMessage(3, "Hello", "Consumer"), topicName);
        kafkaSender.sendMessage("one", "hello", topicName);
        kafkaSender.sendMessage("one two", "hello", topicName);
        kafkaSender.sendMessage("one", "one", topicName);
        kafkaSender.sendMessage("qqqqqqqqqqqqqq qqqqqqqqqqqqqq", "one", topicName);
        kafkaSender.sendMessage("qqqqqqqqqqqqqq", "one", topicName);
        kafkaSender.sendMessage("2222222222222222", "11111111111111111111", topicName);
    }

}
