package com.example.ProducerApp.kafka;

import com.example.ProducerApp.dto.MyMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class KafkaSenderExample {

    final KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(String msg, String key, String topicName) {
        kafkaTemplate.send(topicName, key, msg);
    }
}
